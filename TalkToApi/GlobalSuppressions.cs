﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Estilo", "IDE1006:Estilos de Nomenclatura", Justification = "<Pendente>", Scope = "member", Target = "~M:TalkToApi.V1.Controllers.UsuarioController.Atualizar(System.String,TalkToApi.V1.Models.DTO.UsuarioDTO)~Microsoft.AspNetCore.Mvc.ActionResult")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Qualidade do Código", "IDE0052:Remover membros particulares não lidos", Justification = "<Pendente>", Scope = "member", Target = "~F:TalkToApi.V1.Controllers.MensagemController._mensagemRepository")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Estilo", "IDE0060:Remover o parâmetro não utilizado", Justification = "<Pendente>", Scope = "member", Target = "~M:TalkToApi.V1.Controllers.UsuarioController.ObterUsuario(System.String,Microsoft.AspNetCore.JsonPatch.JsonPatchDocument{TalkToApi.V1.Models.Mensagem},System.String)~Microsoft.AspNetCore.Mvc.ActionResult")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Estilo", "IDE1006:Estilos de Nomenclatura", Justification = "<Pendente>", Scope = "member", Target = "~M:TalkToApi.V1.Controllers.UsuarioController.ObterUsuario(System.String,Microsoft.AspNetCore.JsonPatch.JsonPatchDocument{TalkToApi.V1.Models.Mensagem},System.String)~Microsoft.AspNetCore.Mvc.ActionResult")]

