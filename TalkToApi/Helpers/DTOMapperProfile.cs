﻿using AutoMapper;
using System.Collections.Generic;
using TalkToApi.V1.Models;
using TalkToApi.V1.Models.DTO;

namespace TalkToApi.Helpers {
    public class DTOMapperProfile : Profile
    {
        public DTOMapperProfile()
        {
            CreateMap<ApplicationUser, UsuarioDTO>() 
                .ForMember(dest => dest.Nome, 
                    orig => orig.MapFrom(
                        src => src.FullName)); // Automapper: para que uma propriedade com nome diferente seja tratada como igual.

            CreateMap<Mensagem, MensagemDTO>(); // Automapper: não é necessário mapear individualmente cada propriedade neste caso, pois têm nomes iguais.

            CreateMap<ApplicationUser, UsuarioDTOSenHyperlinks>()
                .ForMember(dest => dest.Nome,
                    orig => orig.MapFrom(
                        src => src.FullName)); // Automapper: para que uma propriedade com nome diferente seja tratada como igual.
            //CreateMap<PaginationList<Palavra>, PaginationList<PalavraDTO>>();
        }
    }
}
