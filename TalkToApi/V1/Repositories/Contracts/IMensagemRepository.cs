﻿using System.Collections.Generic;
using TalkToApi.V1.Models;

namespace TalkToApi.V1.Repositories.Contracts {
    public interface IMensagemRepository {
        List<Mensagem> Obter(string usuarioUmId, string usuarioDoisId);
        Mensagem ObterApenasUm(int id);
        void Cadastrar(Mensagem mensagem);
        void Atualizar(Mensagem mensagem);
    }
}
