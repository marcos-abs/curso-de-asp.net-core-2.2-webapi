﻿using System.ComponentModel.DataAnnotations;

namespace TalkToApi.V1.Models.DTO {
    public class UsuarioDTOSenHyperlinks : BaseDTO {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Slogan { get; set; }
    }
}
