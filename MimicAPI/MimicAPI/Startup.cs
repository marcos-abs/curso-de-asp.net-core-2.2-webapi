﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using MimicAPI.Database;
using MimicAPI.Helpers;
using MimicAPI.Helpers.Swagger;
using MimicAPI.V1.Repositories;
using MimicAPI.V1.Repositories.Contracts;
using System;
using System.IO;
using System.Linq;

namespace MimicAPI {
    public class Startup {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {

            #region AutoMapper-Configuration
            var config = new MapperConfiguration(cfg => { // Configuração do AutoMapper
                cfg.AddProfile(new DTOMapperProfile());
            });
            IMapper mapper = config.CreateMapper(); // Instanciação do AutoMapper
            services.AddSingleton(mapper);          // rodar somente uma única instância/classe para toda a aplicação.
            #endregion

            services.AddDbContext<MimicContext>(opt => {
                opt.UseSqlite("Data Source=Database\\Mimic.db");
            });
            services.AddMvc();
            services.AddScoped<IPalavraRepository, PalavraRepository>();

            #region Ativação do Versionamento
            services.AddApiVersioning(cfg => {
                cfg.ReportApiVersions = true; // ativar a disponibilização do versionamento da API
                cfg.AssumeDefaultVersionWhenUnspecified = true; // parâmetro complementar ao ".DefaultApiVersion"
                cfg.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0); // versão default - padrão ou sugerida.

            });
            #endregion 

            // Após versionamento e documentação com Swagger
            services.AddSwaggerGen(cfg => {

                cfg.ResolveConflictingActions(apiDescription => apiDescription.First()); // para resolver conflitos de versões da API no Swagger, com mesmo nome.

                cfg.SwaggerDoc("v2.0", new Swashbuckle.AspNetCore.Swagger.Info() {
                    Title = "MimicAPI - v2.0",
                    Version = "v2.0"
                });

                cfg.ResolveConflictingActions(apiDescription => apiDescription.First()); // para resolver conflitos de versões da API no Swagger, com mesmo nome.

                cfg.SwaggerDoc("v1.1", new Swashbuckle.AspNetCore.Swagger.Info() {
                    Title = "MimicAPI - v1.1",
                    Version = "v1.1"
                });

                cfg.ResolveConflictingActions(apiDescription => apiDescription.First()); // para resolver conflitos de versões da API no Swagger, com mesmo nome.

                cfg.SwaggerDoc("v1.0", new Swashbuckle.AspNetCore.Swagger.Info() {
                    Title = "MimicAPI - v1.0",
                    Version = "v1.0"
                });

                var CaminhoProjeto = PlatformServices.Default.Application.ApplicationBasePath;
                var NomeProjeto = $"{PlatformServices.Default.Application.ApplicationName}.xml";
                var CaminhoArquivXMLComentario = Path.Combine(CaminhoProjeto, NomeProjeto);

                cfg.IncludeXmlComments(CaminhoArquivXMLComentario);

                cfg.DocInclusionPredicate((docName, apiDesc) => {
                    var actionApiVersionModel = apiDesc.ActionDescriptor?.GetApiVersion();
                    // would mean this action is unversioned and should be included everywhere
                    if (actionApiVersionModel == null) {
                        return true;
                    }
                    if (actionApiVersionModel.DeclaredApiVersions.Any()) {
                        return actionApiVersionModel.DeclaredApiVersions.Any(v => $"v{v.ToString()}" == docName);
                    }
                    return actionApiVersionModel.ImplementedApiVersions.Any(v => $"v{v.ToString()}" == docName);
                });

                cfg.OperationFilter<ApiVersionOperationFilter>(); // Importante para a documentação com versionamento.
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            app.UseStatusCodePages(); // Mensagens de erro amigaveis no browser.

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(cfg => {
                cfg.SwaggerEndpoint("/swagger/v2.0/swagger.json", "MimicAPI v2.0"); // padrão para a documentação com versionamento (Swagger)
                cfg.SwaggerEndpoint("/swagger/v1.1/swagger.json", "MimicAPI v1.1"); // padrão para a documentação com versionamento (Swagger)
                cfg.SwaggerEndpoint("/swagger/v1.0/swagger.json", "MimicAPI v1.0"); // padrão para a documentação com versionamento (Swagger)
                cfg.RoutePrefix = String.Empty; // direcionar para o Swagger qdo acesso direto a rota padrão sem parâmetros.
            });
        }
    }
}
