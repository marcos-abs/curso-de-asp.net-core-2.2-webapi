﻿using Microsoft.AspNetCore.Mvc;
using MimicAPI.Helpers;
using MimicAPI.V1.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MimicAPI.V2.Controllers {
    [ApiController] // necessário para a ativação do versionamento da API.
    [Route("api/v{version:ApiVersion}/[controller]")] // para obter o versionamento na URL
    [ApiVersion("2.0")]
    public class PalavrasController : ControllerBase {
        /// <summary>
        /// Operação que coleta no banco de dados todas as palavras existentes naquele momento.
        /// </summary>
        /// <param name="query">Filtros de pesquisa</param>
        /// <returns>Listagem das palavras</returns>
        [HttpGet("", Name = "ObterTodas")]
        public string ObterTodas() {
            return "Versão 2.0";
        }
    }
}
