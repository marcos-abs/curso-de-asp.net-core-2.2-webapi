﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MimicAPI.V1.Models {
    public class Palavra {
        public int Id { get; set; }
        [Required]
        [MaxLength(150)]
        public string Nome { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Number (greater then zero)")] // [Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")] // ou utilize [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Please enter valid Number")] // [Required] não funciona para campos númericos
        public int Pontuacao { get; set; }
        public bool Ativo { get; set; }
        public DateTime Criado { get; set; }
        public DateTime? Atualizado { get; set; } // data de atualização pode ser "Nula"
    }
}
