﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MimicAPI.Helpers;
using MimicAPI.V1.Models;
using MimicAPI.V1.Models.DTO;
using MimicAPI.V1.Repositories.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MimicAPI.V1.Controllers {
    [ApiController] // necessário para a ativação do versionamento da API.
    [Route("api/v{version:ApiVersion}/[controller]")] // para obter o versionamento na URL
    [ApiVersion("1.0", Deprecated = true)] // Para caso de descontinuar uma versão.
    [ApiVersion("1.1")]
    public class PalavrasController : ControllerBase {
        private readonly IPalavraRepository _repository;
        private readonly IMapper _mapper;

        public PalavrasController(IPalavraRepository repository, IMapper mapper) { // método construtor
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// Operação que coleta no banco de dados todas as palavras existentes naquele momento.
        /// </summary>
        /// <param name="query">Filtros de pesquisa</param>
        /// <returns>Listagem das palavras</returns>
        [MapToApiVersion("1.1")] // declarando que esse método atende a versão 1.1
        [MapToApiVersion("1.0")] // declarando que esse método atende a versão 1.0 deprecated
        [HttpGet("", Name = "ObterTodas")]
        public ActionResult ObterTodas([FromQuery]PalavraUrlQuery query) {

            var item = _repository.ObterPalavras(query);

            if (item.Results.Count == 0) {
                return NotFound();
            }

            PaginationList<PalavraDTO> lista = CriarLinksListPalavraDTO(query, item);

            return Ok(lista);
        }

        /// <summary>
        /// Operação que coleta no banco de dados uma única a palavra.
        /// </summary>
        /// <param name="id">Código identificador do registro (palavra)</param>
        /// <returns>O objeto do registro palavra </returns>
        [MapToApiVersion("1.1")] // declarando que esse método atende a versão 1.1
        [MapToApiVersion("1.0")] // declarando que esse método atende a versão 1.0 deprecated
        [HttpGet("{id}", Name = "ObterPalavra")]
        public ActionResult Obter(int id) {

            var obj = _repository.Obter(id);

            if (obj == null) {
                return NotFound(); // ou return StatusCode(404);
            }

            PalavraDTO palavraDTO = _mapper.Map<Palavra, PalavraDTO>(obj);
            //palavraDTO.Links = new List<LinkDTO>(); // não podemos adicionar informações a uma propriedade nula, por isso precisamos instância-la primeiro.
            adicionarLinkSelf(palavraDTO);
            adicionarLinkUpdate(palavraDTO);
            adicionarLinkDelete(palavraDTO);
            return Ok(palavraDTO);
        }

        /// <summary>
        /// Operação que cadastra palavras (uma por vez) no banco de dados.
        /// </summary>
        /// <param name="palavra">Um objeto palavra</param>
        /// <returns>Um objeto palavra com seu respectivo código identificador (id)</returns>
        [MapToApiVersion("1.1")] // declarando que esse método atende a versão 1.1
        [MapToApiVersion("1.0")] // declarando que esse método atende a versão 1.0 deprecated
        [Route("")]
        [HttpPost]
        public ActionResult Cadastrar([FromBody]Palavra palavra) {

            if (palavra == null) {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return UnprocessableEntity(ModelState); //ou utilizar: return new UnprocessableEntityObjectResult(ModelState); // Erro Status Code 422
            }

            palavra.Ativo = true;
            palavra.Criado = DateTime.Now;

            _repository.Cadastrar(palavra);

            PalavraDTO palavraDTO = _mapper.Map<Palavra, PalavraDTO>(palavra);

            adicionarLinkSelf(palavraDTO);

            return Created($"/api/palavras/{palavra.Id}", palavraDTO);
        }

        /// <summary>
        /// Operação que atualiza/substitui os dados de uma palavra específica no banco de dados.
        /// </summary>
        /// <param name="id">Código identificador do registro (palavra)</param>
        /// <param name="palavra"></param>
        /// <returns>Somente o Status Code</returns>
        [MapToApiVersion("1.1")] // declarando que esse método atende a versão 1.1
        [MapToApiVersion("1.0")] // declarando que esse método atende a versão 1.0 deprecated
        [HttpPut("{id}", Name = "AtualizarPalavra")]
        public ActionResult Atualizar(int id, [FromBody]Palavra palavra) {

            var obj = _repository.Obter(id);

            if (obj == null) {
                return NotFound(); // ou return StatusCode(404);
            }

            if (palavra == null) {
                return BadRequest();
            }

            if (!ModelState.IsValid) {
                return UnprocessableEntity(ModelState); //ou utilizar: return new UnprocessableEntityObjectResult(ModelState); // Erro Status Code 422
            }

            palavra.Id = id;
            palavra.Ativo = obj.Ativo;
            palavra.Criado = obj.Criado;
            palavra.Atualizado = DateTime.Now;
            _repository.Atualizar(palavra);

            PalavraDTO palavraDTO = _mapper.Map<Palavra, PalavraDTO>(obj);
            adicionarLinkSelf(palavraDTO);

            return Ok();
        }

        /// <summary>
        /// Operação que altera o estado (desativa o status) de uma palavra específica no banco de dados.
        /// </summary>
        /// <param name="id">Código identificador do registro (palavra)</param>
        /// <returns>Somente o Status Code</returns>
        [MapToApiVersion("1.1")] // declarando que esse método atende a versão 1.1 somente
        [HttpDelete("{id}", Name = "ExcluirPalavra")]
        public ActionResult Deletar(int id) {

            var palavra = _repository.Obter(id);

            if (palavra == null) {
                return NotFound(); // ou return StatusCode(404);
            }

            _repository.Deletar(id);

            return NoContent(); // ou return Ok();
        }

        private PaginationList<PalavraDTO> CriarLinksListPalavraDTO(PalavraUrlQuery query, PaginationList<Palavra> item) {
            var lista = _mapper.Map<PaginationList<Palavra>, PaginationList<PalavraDTO>>(item);

            foreach (var palavra in lista.Results) {
                palavra.Links = new List<LinkDTO>(); // instanciar antes de adicionar valores, para evitar exceptions.
                palavra.Links.Add(new LinkDTO("self", Url.Link("ObterPalavra", new { id = palavra.Id }), "GET"));
            }

            lista.Links.Add(new LinkDTO("self", Url.Link("ObterTodas", query), "GET"));

            if (item.Paginacao != null) {
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(item.Paginacao));
                if (query.PagNumero + 1 <= item.Paginacao.TotalPaginas) {
                    var queryString = new PalavraUrlQuery() { PagNumero = query.PagNumero + 1, PagRegistro = query.PagRegistro, Data = query.Data };
                    lista.Links.Add(new LinkDTO("next", Url.Link("ObterTodas", queryString), "GET"));
                }
                if (query.PagNumero > 0) {
                    var queryString = new PalavraUrlQuery() { PagNumero = query.PagNumero - 1, PagRegistro = query.PagRegistro, Data = query.Data };
                    lista.Links.Add(new LinkDTO("prev", Url.Link("ObterTodas", queryString), "GET"));
                }
            }

            return lista;
        }

        private void adicionarLinkDelete(PalavraDTO palavraDTO) {
            palavraDTO.Links.Add(
                new LinkDTO("delete", Url.Link("ExcluirPalavra", new { id = palavraDTO.Id }), "DELETE")
            );
        }

        private void adicionarLinkUpdate(PalavraDTO palavraDTO) {
            palavraDTO.Links.Add(
                new LinkDTO("update", Url.Link("AtualizarPalavra", new { id = palavraDTO.Id }), "PUT")
            );
        }

        private void adicionarLinkSelf(PalavraDTO palavraDTO) {
            palavraDTO.Links.Add(
                new LinkDTO("self", Url.Link("ObterPalavra", new { id = palavraDTO.Id }), "GET")
            );
        }
    }
}
