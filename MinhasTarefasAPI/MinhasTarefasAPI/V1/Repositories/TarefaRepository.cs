﻿using MinhasTarefasAPI.Database;
using MinhasTarefasAPI.V1.Models;
using MinhasTarefasAPI.V1.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MinhasTarefasAPI.V1.Repositories {
    public class TarefaRepository : ITarefaRepository {
        private readonly MinhasTarefasContext _banco;

        public TarefaRepository(MinhasTarefasContext banco) {
            _banco = banco;
        }

        public List<Tarefa> Restauracao(ApplicationUser usuario, DateTime dataUltimaSincronizacao) {
            var query = _banco.Tarefas.Where(a => a.UsuarioId == usuario.Id).AsQueryable();
            if (dataUltimaSincronizacao != null) { 
                query.Where(a => a.Criado >= dataUltimaSincronizacao || a.Atualizado >= dataUltimaSincronizacao);
            }
            return query.ToList<Tarefa>();
        }

        public List<Tarefa> Sincronizacao(List<Tarefa> tarefas) {
            var tarefasNovas                = tarefas.Where(a => a.IdTarefaApi == 0).ToList(); // caso for igual a zero => não foi cadastrado no banco de dados ainda.
            var tarefasExcluidasAtualizadas = tarefas.Where(a => a.IdTarefaApi != 0).ToList(); // caso for diferente de  zero => "podem" não ter sido atualizadas no banco de dados ainda.

            if (tarefasNovas.Count() > 0) {
                foreach (var tarefa in tarefasNovas) {
                    _banco.Tarefas.Add(tarefa);
                }
            }
            if (tarefasExcluidasAtualizadas.Count() > 0) {
                foreach (var tarefa in tarefasExcluidasAtualizadas) {
                    _banco.Tarefas.Update(tarefa); //TODO: erro na variável IdTarefaApi 2m48s
                }
            }

            _banco.SaveChanges();

            return tarefasNovas.ToList();
        }
    }
}
