﻿using MinhasTarefasAPI.V1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinhasTarefasAPI.V1.Repositories.Contracts {
    public interface IUsuarioRepository {
        void Cadastrar(ApplicationUser usuario, string senha); //a senha é criptografa, por isso precisa ser passada como argumento.

        ApplicationUser Obter(string email, string senha);
        ApplicationUser Obter(string id);
    }
}
