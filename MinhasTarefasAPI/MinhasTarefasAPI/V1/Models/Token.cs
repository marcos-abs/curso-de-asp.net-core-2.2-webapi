﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinhasTarefasAPI.V1.Models {
    public class Token {
        [Key] // declaração de chave primária. (DataAnnotations)
        public int Id { get; set; }
        public string RefreshToken { get; set; }
        [ForeignKey("Usuario")] // declaração de chave estrangeira. (DataAnnotations)
        public string UsuarioId { get; set; } // relacionamento com a tabela de usuarios.
        public ApplicationUser Usuario { get; set; }
        public bool Utilizado { get; set; }
        public DateTime ExpirationToken { get; set; }
        public DateTime ExpirationRefreshToken { get; set; }
        public DateTime Criado { get; set; }
        public DateTime? Atualizado { get; set; } // Nullable: pode ser criado somente, ou seja, não receber atualizações num primeiro momento.
    }
}
