﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MinhasTarefasAPI.V1.Models {
    public class Tarefa {
        [Key]
        public int IdTarefaApi { get; set; }
        public int IdTarefaApp { get; set; }
        public string Titulo { get; set; }
        public DateTime DataHora { get; set; }
        public string Local { get; set; }
        public string Descricao { get; set; }
        public string Tipo { get; set; }
        public bool Concluido { get; set; } // Estado da tarefa concluido (sim ou não).
        public bool Excluido { get; set; } // Estado da tarefa excluido (sim ou não).
        public DateTime Criado { get; set; }
        public DateTime Atualizado { get; set; }

        [ForeignKey("Usuario")] // Relacionamento => chave estrangeira de UsuarioDTO
        public string UsuarioId { get; set; } // Uma tarefa para um usuario (1to1)
        public virtual ApplicationUser Usuario { get; set; } // vinculo de Tarefa com Usuario
    }
}
