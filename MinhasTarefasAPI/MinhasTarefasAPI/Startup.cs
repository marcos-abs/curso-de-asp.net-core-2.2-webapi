﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MinhasTarefasAPI.Database;
using Microsoft.EntityFrameworkCore;
using MinhasTarefasAPI.V1.Repositories;
using MinhasTarefasAPI.V1.Repositories.Contracts;
using MinhasTarefasAPI.V1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using MinhasTarefasAPI.V1.Helpers.Swagger;
using System;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;

namespace MinhasTarefasAPI {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {

            services.Configure<ApiBehaviorOptions>(op => { // Configurar a ASP.Net Core para acatar as configurações de remoção de campos (ver OBS1de2 e OBS2de2)
                op.SuppressModelStateInvalidFilter = true; // Configurar a ASP.Net Core para acatar as configurações de remoção de campos (ver OBS1de2 e OBS2de2)
            });                                            // Configurar a ASP.Net Core para acatar as configurações de remoção de campos (ver OBS1de2 e OBS2de2)

            services.AddDbContext<MinhasTarefasContext>(op => {
                op.UseSqlite("Data Source=Database\\MinhasTarefas.db");
            });

            #region Respositórios - Controladores
            // Repositórios - Controladores - inicio
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
            services.AddScoped<ITarefaRepository, TarefaRepository>();
            services.AddScoped<ITokenRepository, TokenRepository>();
            // Repositórios - Controladores - final
            #endregion

            services.AddMvc(config => {
                config.ReturnHttpNotAcceptable = true; // Status Code 406.
                config.InputFormatters.Add(new XmlSerializerInputFormatter(config)); // adicionando entradas em XML.
                config.OutputFormatters.Add(new XmlSerializerOutputFormatter()); // adicionando saidas no formato XML.
            })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(
                    options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore //Para evitar o loop infinito no Json
                );

            #region Ativação do Versionamento
            services.AddApiVersioning(cfg => { // para corrigir erro de compatibilidade: instalar o pacote Nuget Microsoft.AspNetCore.MVC.Versioning versão 3.1.6.
                cfg.ReportApiVersions = true; // ativar a disponibilização do versionamento da API
                cfg.AssumeDefaultVersionWhenUnspecified = true; // parâmetro complementar ao ".DefaultApiVersion"
                cfg.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0); // versão default - padrão ou sugerida.

            });
            #endregion
            #region Ativação da documentação com Swagger (após Versionamento)
            services.AddSwaggerGen(cfg => {
                #region Documentaçãoo da Autenticação no Swagger 
                cfg.AddSecurityDefinition("Bearer", new ApiKeyScheme() { // necessário para a autenticação por JWT funcione no Swagger tb.
                    In = "header",
                    Type = "apiKey",
                    Description = "Adicione o JSON Web Token (JWT) para autenticar.",
                    Name = "Authorization"
                });

                var security = new Dictionary<string, IEnumerable<string>>() {
                    { "Bearer", new string[] { } }
                };

                cfg.AddSecurityRequirement(security);
                #endregion

                cfg.ResolveConflictingActions(apiDescription => apiDescription.First()); // para resolver conflitos de versões da API no Swagger, com mesmo nome.

                cfg.SwaggerDoc("v1.0", new Swashbuckle.AspNetCore.Swagger.Info() {
                    Title = "Minhas Tarefas API - v1.0",
                    Version = "v1.0"
                });

                var CaminhoProjeto = PlatformServices.Default.Application.ApplicationBasePath; // para resolver erro instalar pacote Nuget Microsoft.Extensions.PlatformAbastractions.
                var NomeProjeto = $"{PlatformServices.Default.Application.ApplicationName}.xml";
                var CaminhoArquivXMLComentario = Path.Combine(CaminhoProjeto, NomeProjeto);

                cfg.IncludeXmlComments(CaminhoArquivXMLComentario);

                cfg.DocInclusionPredicate((docName, apiDesc) => {
                    var actionApiVersionModel = apiDesc.ActionDescriptor?.GetApiVersion(); //todo: parei aqui 13m24s - 26. [ESTUDO] Versionamento e Swagger - Parte 1.
                    // would mean this action is unversioned and should be included everywhere
                    if (actionApiVersionModel == null) {
                        return true;
                    }
                    if (actionApiVersionModel.DeclaredApiVersions.Any()) {
                        return actionApiVersionModel.DeclaredApiVersions.Any(v => $"v{v.ToString()}" == docName);
                    }
                    return actionApiVersionModel.ImplementedApiVersions.Any(v => $"v{v.ToString()}" == docName);
                });

                cfg.OperationFilter<ApiVersionOperationFilter>(); // Importante para a documentação com versionamento.
            });

            #endregion
            // Adicionando o EntityFramework como serviço ao projeto.
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<MinhasTarefasContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options => {
                options.TokenValidationParameters = new TokenValidationParameters() {
                    ValidateIssuer = false, // poderia ser o domínio da API (recomendação do Prof. Elias)
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("chave-api-jwt-minhas-tarefas"))
                };
            });

            services.AddAuthorization(auth => {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build()
                    );
            });

            services.ConfigureApplicationCookie(options => {
                options.Events.OnRedirectToLogin = context => {
                    context.Response.StatusCode = 401;
                    return Task.CompletedTask;
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStatusCodePages(); // Utilizar os códigos de estado das páginas HTTP.
            app.UseAuthentication(); // necessário para ativar a utilização do token
            app.UseHttpsRedirection();
            app.UseMvc();

            #region Ativação do Swagger
            app.UseSwagger();
            app.UseSwaggerUI(cfg => {
                cfg.SwaggerEndpoint("/swagger/v1.0/swagger.json", "Minhas Tarefas API v1.0"); // padrão para a documentação com versionamento (Swagger)
                cfg.RoutePrefix = String.Empty; // direcionar para o Swagger qdo acesso direto a rota padrão sem parâmetros.
            });
            #endregion
        }
    }
}
