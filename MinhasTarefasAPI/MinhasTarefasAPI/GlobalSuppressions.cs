﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Estilo", "IDE0060:Remover o parâmetro não utilizado", Justification = "<Pendente>", Scope = "member", Target = "~M:MinhasTarefasAPI.Controllers.UsuarioController.Login(MinhasTarefasAPI.Models.UsuarioDTO)~Microsoft.AspNetCore.Mvc.ActionResult")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Estilo", "IDE1006:Estilos de Nomenclatura", Justification = "<Pendente>", Scope = "member", Target = "~M:MinhasTarefasAPI.Controllers.UsuarioController.Login(MinhasTarefasAPI.Models.UsuarioDTO)~Microsoft.AspNetCore.Mvc.ActionResult")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Estilo", "IDE1006:Estilos de Nomenclatura", Justification = "<Pendente>", Scope = "member", Target = "~M:MinhasTarefasAPI.Controllers.UsuarioController.BuildToken(MinhasTarefasAPI.Models.ApplicationUser)~System.String")]

