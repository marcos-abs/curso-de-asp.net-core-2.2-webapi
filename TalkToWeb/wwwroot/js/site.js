﻿function TesteCors() {
    var tokenJWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Impvc2VAZ21haWwuY29tIiwic3ViIjoiNDJiZGM2NTEtOTFiNC00OTM0LWJiYjktNjRkOWU3YThhMzZlIiwiZXhwIjoxNTg1Njc1OTkyfQ.8fta8ho5uUViyfeLSYDnmIqYs2eGnHllhUPK8a-Kl-I";
    var servico = "https://localhost:44327/api/mensagem/42bdc651-91b4-4934-bbb9-64d9e7a8a36e/f4663734-06ba-4a40-9c63-772041e711b0";

    $("#resultado").html("---Solicitando---");

    $.ajax({
        url: servico,
        method: "GET",
        crossDomain: true,
        headers: {
            "Accept": "application/json"
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + tokenJWT);
        },
        success: function (data, status, xhr) {
            $("#resultado").html(data);
            console.info(data);
        }
    });
}